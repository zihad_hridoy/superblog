<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
	public function index(){
		$categories = Category::all();
		return response()->json($categories);
	}

    public function store(Request $request){
    	$this->validate($request, [
    		'cat_name' => 'required'
    	]);	

    	$category = new Category();
    	$category->cat_name = $request->cat_name;
    	$category->save();
    	return ['message' => 'OK'];
    }
}
