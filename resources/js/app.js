
require('./bootstrap');

window.Vue = require('vue');

//vuex
import Vuex from 'vuex'
Vue.use(Vuex)
import vuex_store from "./vuex/index"
const store = new  Vuex.Store(vuex_store)

//vue router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import {routes} from './routes';

const router = new VueRouter({
  routes,
  mode : 'hash'
})

//component
Vue.component('admin-main', require('./components/admin/AdminMaster.vue').default);

// v-form
import { Form, HasError, AlertError } from 'vform'
//for global use
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)


//sweet aleart
import Swal from 'sweetalert2'
window.Swal = Swal;

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.Toast = Toast

const app = new Vue({
    el: '#app',
    router,
    store
});

