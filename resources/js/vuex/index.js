export default {
	state: {
		category: [],
	},
	getters:{
		getCategory(state){
			return state.category
		}
	},
	actions:{
		allCategory(context){
			axios.get("/category_list")
				 .then((response)=> {
				 	context.commit('categories', response.data)
				 })

		}
	},
	mutations:{
		categories(state, payload){
			return this.state.category = payload
		}
	}
}