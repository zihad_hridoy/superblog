@include('admin.includes.header')
<!-- Site wrapper -->

<div class="wrapper" id="app">

  @include('admin.includes.nav')

  <div class="content-wrapper">
    <section class="content">
      <admin-main></admin-main>
    </section>
  </div> 
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
